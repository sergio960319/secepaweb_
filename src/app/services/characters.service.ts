import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Character } from '../models/character';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  API_URI = 'http://localhost:3000/api';

  constructor(protected http: HttpClient) { }

  getCharacter() {
    return this.http.get(`${this.API_URI}/characters`);
  }

  deleteCharacter(id: string) {
    return this.http.delete(`${this.API_URI}/characters/${id}`);
  }

  saveCharacter(character: Character) {
    return this.http.post(`${this.API_URI}/characters`, character);
  }

  updateCharacter(id: string|number, updateCharacter: Character): Observable<any> {
    return this.http.put(`${this.API_URI}/characters/${id}`, updateCharacter);
  }


}
