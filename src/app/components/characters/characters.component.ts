import { Component, OnInit } from '@angular/core';

import { CharactersService } from 'src/app/services/characters.service';
import { Character } from 'src/app/models/character';
import { empty } from 'rxjs';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
})
export class CharactersComponent implements OnInit {

  constructor(protected characterService: CharactersService) { }
  accion: string;
  update: Boolean = false;
  Form: Boolean = false;
  ButtomAddCharacter: Boolean = true;
  DeleteButton: Boolean = false;
  formValid: Boolean = true;
  characters: any = [];
  fcharacter: any = new Character();

  CreateCharacter() {
    this.formValidation();

    if (this.formValid) {
      this.postCharacter();
    }
  }


  UpdateCharacter() {
    this.formValidation();
    if (this.formValid) {
      this.putCharacter();
    }

  }

  showForm() {
    this.Form = true;
    this.ButtomAddCharacter = false;
    this.accion = 'Crear personaje';
  }

  Select(selcharacter: Character) {
    this.fcharacter = selcharacter;
    this.Form = true;
    this.DeleteButton = true;
    this.update = true;
    this.ButtomAddCharacter = false;
    this.accion = 'Actualizar';
  }

  ngOnInit() {
    this.getCharacters();
  }

  getCharacters() {
    this.characterService.getCharacter().subscribe(res => {
      this.characters = res;
    },
      (error) => {
      }
    );
  }

  putCharacter() {
    this.characterService.updateCharacter(this.fcharacter.id, this.fcharacter).subscribe(res => {
      this.getCharacters();
      this.restartflags();
    },
      err => console.error(err)
    );

  }

  delete(id: string) {
    this.characterService.deleteCharacter(id)
      .subscribe(
        res => {
          this.getCharacters();
          this.restartflags();
        },
        err => console.error(err)
      );
  }

  postCharacter() {
    this.characterService.saveCharacter(this.fcharacter).subscribe(res => {
      this.getCharacters();
      this.restartflags();
    },
      err => console.error(err)
    );

  }

  formValidation() {
    console.log(this.fcharacter.nombre);
    if (this.fcharacter.nombre !== undefined || this.fcharacter.raza !== undefined || this.fcharacter.edad !== undefined
      || this.fcharacter.aspecto !== undefined || this.fcharacter.historia !== undefined || this.fcharacter.habilidades !== undefined) {
      this.formValid = true;
      console.log(this.formValid);

    } else {
      this.formValid = false;
      console.log(this.formValid);
    }
  }

  restartflags() {
    this.Form = false;
    this.ButtomAddCharacter = true;
    this.formValid = true;
    this.DeleteButton = false;
    this.fcharacter = empty;

  }


}
