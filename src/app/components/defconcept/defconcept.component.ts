import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-defconcept',
  templateUrl: './defconcept.component.html',
  styleUrls: ['./defconcept.component.css']
})
export class DefconceptComponent implements OnInit {

  gameaspect: Boolean;
  gamename: string;
  gameplay: Boolean;
  genre: Boolean;
  players: Boolean;
  Mensaje: string;


  constructor() {   }

save() {
  if (this.gameaspect&&!this.gameplay&&!this.genre&&!this.players) {
    this.Mensaje = 'Aspecto del Juego';
  }
  this.back();
}


  ngOnInit() {
  }

  ShowAspectGame(): void {
    this.Mensaje = 'Aspecto del Juego';
    this.gameaspect = true;
  }
  ModGameplay(): void {
    this.Mensaje = 'Gameplay';
    this.gameplay = true;
    this.genre = false;
    this.players = false;

  }

  clear() {
    this.Mensaje = '';
  }

ModGenre(): void {
  this.Mensaje = 'Género';
    this.gameplay = false;
    this.genre = true;
    this.players = false;
  }

ModPlayers(): void {
  this.Mensaje = 'Personajes';
    this.gameplay = false;
    this.genre = false;
    this.players = true;
  }

back(): void {
  if (!this.gameplay && !this.genre && !this.players) {
    this.gameaspect = false;
  }
  this.gameplay = false;
  this.genre = false;
  this.players = false;
}






}
