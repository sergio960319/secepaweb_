export class Character {
  id: number;
  nombre: string;
  raza: string;
  edad: string;
  habilidades: string;
  aspecto: string;
  historia: string;
}
