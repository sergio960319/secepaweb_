import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { APP_ROUTING } from './app.routes';
import { HttpClientModule} from '@angular/common/http';


// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { DefconceptComponent } from './components/defconcept/defconcept.component';
import { GameplayComponent } from './components/gameplay/gameplay.component';
import { GenreComponent } from './components/genre/genre.component';
import { CharactersService } from './services/characters.service';
import { CharactersComponent } from './components/characters/characters.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    MenuComponent,
    DefconceptComponent,
    GameplayComponent,
    GenreComponent,
    CharactersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    // tslint:disable-next-line: deprecation
    HttpModule,
    APP_ROUTING,
    HttpClientModule
  ],
  providers: [
    CharactersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
