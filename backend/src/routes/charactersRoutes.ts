import express, { Router } from 'express';
import charactersController from '../controllers/charactersController';

class CharacterRoutes {

    router: Router = Router();

    constructor() {
        this.config();
    }

    config() {
        this.router.get('/', charactersController.list);
        this.router.post('/', charactersController.create);
        this.router.put('/:id', charactersController.update);
        this.router.delete('/:id', charactersController.delete);

    }

}

export default new CharacterRoutes().router;

