import { Request, Response } from 'express';
import pool from '../database';

class characterController {

    public async list(req: Request, res: Response): Promise<void> {
        const characters = await pool.query('SELECT * FROM characters');
        res.json(characters);
    }

 
    public async create(req: Request, res: Response): Promise<void> {
        console.log(req);
        const result = await pool.query('INSERT INTO characters set ?', [req.body]);
        res.json({ message: 'Game Saved' });
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const oldGame = req.body;
        await pool.query('UPDATE characters set ? WHERE id = ?', [req.body, id]);
        res.json({ message: "The game was Updated" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('DELETE FROM characters WHERE id = ?', [id]);
        res.json({ message: "The game was deleted" });
    }
}

const charactersController = new characterController;
export default charactersController;