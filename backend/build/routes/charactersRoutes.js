"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const charactersController_1 = __importDefault(require("../controllers/charactersController"));
class CharacterRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', charactersController_1.default.list);
        this.router.post('/', charactersController_1.default.create);
        this.router.put('/:id', charactersController_1.default.update);
        this.router.delete('/:id', charactersController_1.default.delete);
    }
}
exports.default = new CharacterRoutes().router;
